<?php

namespace Drupal\automatic_updates\Validator;

use Drupal\automatic_updates\ConsoleUpdateStage;
use Drupal\package_manager\Event\PreCreateEvent;
use Drupal\package_manager\Event\StatusCheckEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Validates that the `auto-update` script is not run as the root user.
 *
 * We add this warning to the StatusCheckEvent and PreCreateEvent events
 * instead of directly in the `auto-update` script to ensure that the warning
 * is surfaced to the status report page along with other warnings and errors.
 *
 * @todo Remove this validator in favor of exiting the `auto-update` script as
 *   early as possible if run as root in https://www.drupal.org/i/3432496.
 */
class ConsoleUserValidator implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      StatusCheckEvent::class => ['validateConsoleUser'],
      PreCreateEvent::class => ['validateConsoleUser'],
    ];
  }

  /**
   * Adds a warning if the `auto-update` script is run as the root user.
   *
   * @param \Drupal\package_manager\Event\StatusCheckEvent|\Drupal\package_manager\Event\PreCreateEvent $event
   *   The stage event.
   */
  public function validateConsoleUser(StatusCheckEvent|PreCreateEvent $event) {
    if (PHP_SAPI === 'cli' && $event->stage instanceof ConsoleUpdateStage && function_exists('posix_getuid') && posix_getuid() === 0) {
      $event->addWarning([t('The `auto-update` script should not be run as the root user. Please run it as a less privileged user. In 3.1.0 if this script is run as the root user updates will not be preformed.')]);
    }
  }

}
